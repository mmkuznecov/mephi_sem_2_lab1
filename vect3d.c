#include "vect3d.h"
#include <assert.h>



// init funcs for ELs

EL float_init_EL(){
    float *value = (float*)malloc(sizeof(float)); // allicate mem
    EL result = {value, float_sum, float_mult, float_sub}; // init
    return result;
}

EL complex_init_EL(){
    complex *value = (complex*)malloc(sizeof(complex));
    EL result = {value, complex_sum, complex_mult, complex_sub};
    return result;
}

// arithmetic funcs

EL float_sum(EL c1, EL c2){
    assert((&c1 != NULL) && (&c2 != NULL)); // check args
    float *sum_ = (float*)malloc(sizeof(float)); // mem allocation
    *sum_ =  *(float*)c1.data + *(float*)c2.data; // result
    EL result_c = float_init_EL();
    result_c.data = sum_;
    return result_c;
}

EL float_sub(EL c1, EL c2){
    assert((&c1 != NULL) && (&c2 != NULL));
    float *sub_ = (float*)malloc(sizeof(float));
    *sub_ = *(float*)c1.data - *(float*)c2.data;
    EL result_c = float_init_EL();
    result_c.data = sub_;
    return result_c;
}

EL complex_sum(EL c1, EL c2){
    assert((&c1 != NULL) && (&c2 != NULL));
    complex *sum_ = (complex*)malloc(sizeof(complex));
    (*sum_).I = (*(complex*)c1.data).I + (*(complex*)c2.data).I;
    (*sum_).R = (*(complex*)c1.data).R + (*(complex*)c2.data).R;
    EL result_c = complex_init_EL();
    result_c.data = sum_;
    return result_c;
}


EL complex_sub(EL c1, EL c2){
    assert((&c1 != NULL) && (&c2 != NULL));
    complex *sub_ = (complex*)malloc(sizeof(complex));
    (*sub_).I = (*(complex*)c1.data).I - (*(complex*)c2.data).I;
    (*sub_).R = (*(complex*)c1.data).R - (*(complex*)c2.data).R;
    EL result_c = complex_init_EL();
    result_c.data = sub_;
    return result_c;
}

EL float_mult(EL c1, EL c2){
    assert((&c1 != NULL) && (&c2 != NULL));
    float *mult_ = (float*)malloc(sizeof(float));
    *mult_ = *(float*)c1.data * *(float*)c2.data;
    EL result_c = float_init_EL();
    result_c.data = mult_;
    return result_c;
}

EL complex_mult(EL c1, EL c2){
    assert((&c1 != NULL) && (&c2 != NULL));
    complex *mult_ = (complex*)malloc(sizeof(complex));
    float I1 = (*(complex*)c1.data).I;
    float R1 = (*(complex*)c1.data).R;
    float I2 = (*(complex*)c2.data).I;
    float R2 = (*(complex*)c2.data).R;

    (*mult_).R = R1 * R2 - I1 * I2;
    (*mult_).I = R1 * I2 + R2 * I1;
    EL result_c = complex_init_EL();
    result_c.data = mult_;
    return result_c;
}

// destructors to clean memory

void complex_EL_destructor(EL complex_el){
    free(complex_el.data);
}

void float_EL_destructor(EL float_el){
    free(float_el.data);
}