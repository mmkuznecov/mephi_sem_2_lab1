#include "vect_funcs.c"
#include <string.h> // used for memcpy

// pretty print funcs for UI

void float_pretty_print(float x, float y, float z){
    printf("(%f;%f;%f)\n", x, y, z);
}

void complex_pretty_print(float x, float xi, float y, float yi, float z, float zi){
    printf("({%f , %f};{%f , %f};{%f , %f})\n", x, xi, y, yi, z, zi);
}

int main(){

    int choose; // choose of type of vector: FLOAT or COMPLEX
    float *values; // input values
    printf("Input type of vectors: 0 - floats, 1 - complex numbers.\n");
    scanf("%d", &choose);

    EL X1, Y1, Z1, X2, Y2, Z2; // elements of input vectors

    if (choose){
    
        float * values;
        values = (float*)malloc(sizeof(float)*12);
        printf("Input first vector as 6 values (R and Im for x,y,z):\n");
        scanf("%f %f %f %f %f %f", &values[0], &values[1], &values[2], &values[3], &values[4], &values[5]);

        printf("Input second vector as 6 values (R and Im for x,y,z):\n");
        scanf("%f %f %f %f %f %f", &values[6], &values[7], &values[8], &values[9], &values[10], &values[11]);


        printf("First vector:");
        complex_pretty_print(values[0], values[1], values[2], values[3], values[4], values[5]);
        printf("Second vector:");
        complex_pretty_print(values[6], values[7], values[8], values[9], values[10], values[11]);


        // init elements

        X1 = complex_init_EL();
        Y1 = complex_init_EL();
        Z1 = complex_init_EL();
        X2 = complex_init_EL();
        Y2 = complex_init_EL();
        Z2 = complex_init_EL();


        complex x1 = {values[0], values[1]};
        complex y1 = {values[2], values[3]};
        complex z1 = {values[4], values[5]};
        complex x2 = {values[6], values[7]};
        complex y2 = {values[8], values[9]};
        complex z2 = {values[10], values[11]};


        memcpy(X1.data, &x1, sizeof(complex));
        memcpy(Y1.data, &y1, sizeof(complex));
        memcpy(Z1.data, &z1, sizeof(complex));
        memcpy(X2.data, &x2, sizeof(complex));
        memcpy(Y2.data, &y2, sizeof(complex));
        memcpy(Z2.data, &z2, sizeof(complex));


        vector3d v1 = init_complex_vector();
        vector3d v2 = init_complex_vector();

        v1.x = X1;
        v1.y = Y1;
        v1.z = Z1;

        v2.x = X2;
        v2.y = Y2;
        v2.z = Z2;

        vector3d vect_s, vect_m;
        EL scalar_m = complex_init_EL();

        vect_s = vect_sum(v1, v2, 1);
        vect_m = vect_mult(v1, v2, 1);
        scalar_m = scalar_mult(v1, v2, 1);

        printf("Vector sum: ");
        complex_pretty_print((*(complex*)vect_s.x.data).R, (*(complex*)vect_s.x.data).I, (*(complex*)vect_s.y.data).R, (*(complex*)vect_s.y.data).I,(*(complex*)vect_s.z.data).R, (*(complex*)vect_s.z.data).I);
        printf("Vector multiplication: ");
        complex_pretty_print((*(complex*)vect_m.x.data).R, (*(complex*)vect_m.x.data).I, (*(complex*)vect_m.y.data).R, (*(complex*)vect_m.y.data).I,(*(complex*)vect_m.z.data).R, (*(complex*)vect_m.z.data).I);
        printf("Scalar multiplication: {%f;%f}\n", (*(complex*)scalar_m.data).R, (*(complex*)scalar_m.data).I);


        destroy_complex_vector(v1);
        destroy_complex_vector(v2);
        destroy_complex_vector(vect_s);
        destroy_complex_vector(vect_m);
        complex_EL_destructor(scalar_m);
    }
    else{

        values = (float*)malloc(sizeof(float)*6);
        printf("Input first vector as 3 values for x,y,z:\n");
        scanf("%f %f %f", &values[0], &values[1], &values[2]);
        
        printf("Input second vector as 3 values for x,y,z:\n");
        scanf("%f %f %f", &values[3], &values[4], &values[5]);

        printf("First vector:");
        float_pretty_print(values[0], values[1], values[2]);
        printf("Second vector:");
        float_pretty_print(values[3], values[4], values[5]);
        

        X1 = float_init_EL();
        Y1 = float_init_EL();
        Z1 = float_init_EL();
        X2 = float_init_EL();
        Y2 = float_init_EL();
        Z2 = float_init_EL();

        memcpy(X1.data, &(values[0]), sizeof(float));
        memcpy(Y1.data, &(values[1]), sizeof(float));
        memcpy(Z1.data, &(values[2]), sizeof(float));
        memcpy(X2.data, &(values[3]), sizeof(float));
        memcpy(Y2.data, &(values[4]), sizeof(float));
        memcpy(Z2.data, &(values[5]), sizeof(float));


        vector3d v1 = init_float_vector();
        vector3d v2 = init_float_vector();

        v1.x = X1;
        v1.y = Y1;
        v1.z = Z1;

        v2.x = X2;
        v2.y = Y2;
        v2.z = Z2;

        vector3d vect_s, vect_m;
        EL scalar_m = float_init_EL();

        vect_s = vect_sum(v1, v2, 0);
        vect_m = vect_mult(v1, v2, 0);
        scalar_m = scalar_mult(v1, v2, 0);

        printf("Vector sum: ");
        float_pretty_print(*(float*)vect_s.x.data, *(float*)vect_s.y.data, *(float*)vect_s.z.data);
        printf("Vector multiplication: ");
        float_pretty_print(*(float*)vect_m.x.data, *(float*)vect_m.y.data, *(float*)vect_m.z.data);
        printf("Scalar multiplication: %f\n", *(float*)scalar_m.data);

        destroy_float_vector(v1);
        destroy_float_vector(v2);
        destroy_float_vector(vect_s);
        destroy_float_vector(vect_m);
        float_EL_destructor(scalar_m);

        free(values);

    }

}