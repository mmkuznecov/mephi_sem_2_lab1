#include "vect3d.c"
#include <assert.h>

// vector init funcs
vector3d init_float_vector();
vector3d init_complex_vector();

// vector calculation funcs
vector3d vect_sum(vector3d v1, vector3d v2, int type);
EL scalar_mult(vector3d v1, vector3d v2, int type);
vector3d vect_mult(vector3d v1, vector3d v2, int type);


// vector destructor funcs to clean memory
void destroy_complex_vector(vector3d complex_vect);
void destroy_float_vector(vector3d float_vect);

// vect init, depending on type of content

vector3d init_float_vector(){
    vector3d vect;
    EL x = float_init_EL();
    EL y = float_init_EL();
    EL z = float_init_EL();

    vect.x = x;
    vect.y = y;
    vect.z = z;

    return vect;
}

vector3d init_complex_vector(){
    vector3d vect;
    EL x = complex_init_EL();
    EL y = complex_init_EL();
    EL z = complex_init_EL();

    vect.x = x;
    vect.y = y;
    vect.z = z;

    return vect;
}

vector3d vect_sum(vector3d v1, vector3d v2, int type){
    assert((&v1 != NULL) && (&v2 != NULL)); // argument correctness checking

    vector3d result_vec;
    if (type){
        result_vec = init_complex_vector();
    }
    else
    {
        result_vec = init_float_vector();
    }

    result_vec.x = result_vec.x.sum(v1.x, v2.x);
    
    result_vec.y = result_vec.y.sum(v1.y, v2.y);
    
    result_vec.z = result_vec.z.sum(v1.z, v2.z);
    
    return result_vec;    
}

EL scalar_mult(vector3d v1, vector3d v2, int type){
    assert((&v1 != NULL) && (&v2 != NULL));

    EL result_el, mx, my, mz;
    if (type){
        result_el = complex_init_EL();
        mx = complex_init_EL();
        my = complex_init_EL();
        mz = complex_init_EL();
    }
    else
    {
        result_el = float_init_EL();
        mx = float_init_EL();
        my = float_init_EL();
        mz = float_init_EL();
    }

    mx = result_el.mult(v1.x, v2.x);
    my = result_el.mult(v1.y, v2.y);
    mz = result_el.mult(v1.z, v2.z);

    result_el = result_el.sum(result_el.sum(mx, my), mz);

    return result_el;    
}

vector3d vect_mult(vector3d v1, vector3d v2, int type){
    assert((&v1 != NULL) && (&v2 != NULL));

    vector3d result_vec;
    if (type){
        result_vec = init_complex_vector();
    }
    else
    {
        result_vec = init_float_vector();
    }

    result_vec.x = result_vec.x.sub(result_vec.x.mult(v1.y, v2.z), result_vec.x.mult(v2.y, v1.z));
    result_vec.y = result_vec.y.sub(result_vec.y.mult(v2.x, v1.z), result_vec.y.mult(v1.x, v2.z));
    result_vec.z = result_vec.z.sub(result_vec.z.mult(v1.x, v2.y), result_vec.z.mult(v2.x, v1.y));

    return result_vec;
}

// destructor funcs

void destroy_float_vector(vector3d float_vect){
    float_EL_destructor(float_vect.x);
    float_EL_destructor(float_vect.y);
    float_EL_destructor(float_vect.z);
}

void destroy_complex_vector(vector3d complex_vect){
    complex_EL_destructor(complex_vect.x);
    complex_EL_destructor(complex_vect.y);
    complex_EL_destructor(complex_vect.z);
}