#include <stdio.h>
#include <stdlib.h>
#include "vect_funcs.c"

// read line for float vects

void read_floats(FILE *file, float * float_vals){
    int i;
    for (i = 0; i < 13; i++){
        fscanf(file, "%f", &float_vals[i]);
    }
}

// read line for complex vects

void read_complex(FILE *file, float * complex_vals){
    int i;
    for (i = 0; i < 26; i++){
        fscanf(file, "%f", &complex_vals[i]);
    }
}

// test for floats

int check_float(float *vals){


    EL X1, Y1, Z1, X2, Y2, Z2;

    X1 = float_init_EL();
    Y1 = float_init_EL();
    Z1 = float_init_EL();
    X2 = float_init_EL();
    Y2 = float_init_EL();
    Z2 = float_init_EL();

    X1.data = &vals[0];
    Y1.data = &vals[1];
    Z1.data = &vals[2];
    X2.data = &vals[3];
    Y2.data = &vals[4];
    Z2.data = &vals[5];

    vector3d v1 = init_float_vector();
    vector3d v2 = init_float_vector();

    v1.x = X1;
    v1.y = Y1;
    v1.z = Z1;

    v2.x = X2;
    v2.y = Y2;
    v2.z = Z2;

    vector3d vect_s, vect_m;
    EL scalar_m = float_init_EL();

    vect_s = vect_sum(v1, v2, 0);
    vect_m = vect_mult(v1, v2, 0);
    scalar_m = scalar_mult(v1, v2, 0);

    float vsx_ = *(float*)vect_s.x.data;
    float vsy_ = *(float*)vect_s.y.data;
    float vsz_ = *(float*)vect_s.z.data;

    float vmx_ = *(float*)vect_m.x.data;
    float vmy_ = *(float*)vect_m.y.data;
    float vmz_ = *(float*)vect_m.z.data;

    float sm_ = *(float*)scalar_m.data;

    if ((vals[6] == vsx_ ) && (vals[7] == vsy_ ) && (vals[8] == vsz_ ) && (vals[12] == sm_ ) && (vals[9] == vmx_ ) && (vals[10] == vmy_ ) && (vals[11] == vmz_ )){
        return 1;
    }
    else{
        return 0;
    }
    
}

// test for complex

int check_complex(float *vals){
    EL X1, Y1, Z1, X2, Y2, Z2;

    X1 = complex_init_EL();
    Y1 = complex_init_EL();
    Z1 = complex_init_EL();
    X2 = complex_init_EL();
    Y2 = complex_init_EL();
    Z2 = complex_init_EL();


    complex x1 = {vals[0], vals[1]};
    complex y1 = {vals[2], vals[3]};
    complex z1 = {vals[4], vals[5]};
    complex x2 = {vals[6], vals[7]};
    complex y2 = {vals[8], vals[9]};
    complex z2 = {vals[10], vals[11]};

    X1.data = &x1;
    Y1.data = &y1;
    Z1.data = &z1;
    X2.data = &x2;
    Y2.data = &y2;
    Z2.data = &z2;

    vector3d v1 = init_complex_vector();
    vector3d v2 = init_complex_vector();

    v1.x = X1;
    v1.y = Y1;
    v1.z = Z1;

    v2.x = X2;
    v2.y = Y2;
    v2.z = Z2;

    vector3d vect_s, vect_m;
    EL scalar_m = complex_init_EL();

    vect_s = vect_sum(v1, v2, 1);
    vect_m = vect_mult(v1, v2, 1);
    scalar_m = scalar_mult(v1, v2, 1);

    float vsxr = (*(complex*)vect_s.x.data).R;
    float vsxi = (*(complex*)vect_s.x.data).I;

    float vsyr = (*(complex*)vect_s.y.data).R;
    float vsyi = (*(complex*)vect_s.y.data).I;

    float vszr = (*(complex*)vect_s.z.data).R;
    float vszi = (*(complex*)vect_s.z.data).I;


    float vmxr = (*(complex*)vect_m.x.data).R;
    float vmxi = (*(complex*)vect_m.x.data).I;

    float vmyr = (*(complex*)vect_m.y.data).R;
    float vmyi = (*(complex*)vect_m.y.data).I;

    float vmzr = (*(complex*)vect_m.z.data).R;
    float vmzi = (*(complex*)vect_m.z.data).I;

    float smr = (*(complex*)scalar_m.data).R;
    float smi = (*(complex*)scalar_m.data).I;

    if ((vsxr == vals[12]) && (vsxi == vals[13]) && (vsyr == vals[14]) && (vsyi == vals[15]) && (vszr == vals[16]) && (vszi == vals[17]) && 
        (vmxr == vals[18]) && (vmxi == vals[19]) && (vmyr == vals[20]) && (vmyi == vals[21]) && (vmzr == vals[22]) && (vmzi == vals[23]) &&
        (smr == vals[24]) && (smi == vals[25])){
        return 1;
    }
    else{
        return 0;
    }
}



int main(void){
    int i, j;
    int choice;
    float correct = 0; // number of correct tests
    float all = 0; // number of all tests
    float *float_vals; // vals for float tests
    float *complex_vals; // vals for complex tests

    float_vals = (float*)malloc(sizeof(float)*13);
    complex_vals = (float*)malloc(sizeof(complex)*26);

    FILE* file = fopen ("tests.txt", "r"); // file with tests


    while (!feof(file))
    {
        all++;
        fscanf(file, "%d", &choice);
        if (!choice){
            read_floats(file, float_vals);
            if (check_float(float_vals)){
                printf("SUCCESS ON TEST %d\n", (int)all);
                correct++;
            }
            else{
                printf("ERROR ON TEST %d\n", (int)all);
            }
        }
        else
        {
            read_complex(file, complex_vals);
            if (check_complex(complex_vals)){
                printf("SUCCESS ON TEST %d\n", (int)all);
                correct++;
            }
            else{
                printf("ERROR ON TEST %d\n", (int)all);
            }
        }
        
    }
    float percentage = correct / all; // percentage of correct tests

    printf("------------------------------------\n");
    printf("%.2f %% OF TESTS ARE SUCCESSFUL\n", percentage*100);

    fclose(file);
    
    return 0;
}