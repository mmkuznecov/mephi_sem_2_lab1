#include <stdio.h>
#include <stdlib.h>
#include "vect_funcs.c"

void float_pretty_print(float x, float y, float z){
    printf("(%f;%f;%f)\n", x, y, z);
}

void complex_pretty_print(float x, float xi, float y, float yi, float z, float zi){
    printf("({%f , %f};{%f , %f};{%f , %f})\n", x, xi, y, yi, z, zi);
}

int main(){
    int base_size = 6;
    int choose;
    float *values;
    printf("Input type of vectors: 0 - floats, 1 - complex numbers.\n");
    scanf("%d", &choose);

    EL X1, Y1, Z1, X2, Y2, Z2;


    if (choose){
        values = (float*)malloc(base_size*2);
        printf("Input first vector as 6 values (R and Im for x,y,z):\n");
        scanf("%f %f %f %f %f %f", &values[0], &values[1], &values[2], &values[3], &values[4], &values[5]);

        printf("Input second vector as 6 values (R and Im for x,y,z):\n");
        scanf("%f %f %f %f %f %f", &values[6], &values[7], &values[8], &values[9], &values[10], &values[11]);

        printf("First vector:");
        complex_pretty_print(values[0], values[1], values[2], values[3], values[4], values[5]);
        printf("Second vector:");
        complex_pretty_print(values[6], values[7], values[8], values[9], values[10], values[11]);

        X1 = complex_init_EL();
        Y1 = complex_init_EL();
        Z1 = complex_init_EL();
        X2 = complex_init_EL();
        Y2 = complex_init_EL();
        Z2 = complex_init_EL();


        complex x1 = {values[0], values[1]};
        complex y1 = {values[2], values[3]};
        complex z1 = {values[4], values[5]};
        complex x2 = {values[6], values[7]};
        complex y2 = {values[8], values[9]};
        complex z2 = {values[10], values[11]};

        X1.data = &x1;
        Y1.data = &y1;
        Z1.data = &z1;
        X2.data = &x2;
        Y2.data = &y2;
        Z2.data = &z2;

        vector3d v1 = init_complex_vector();
        vector3d v2 = init_complex_vector();

        v1.x = X1;
        v1.y = Y1;
        v1.z = Z1;

        v2.x = X2;
        v2.y = Y2;
        v2.z = Z2;

        vector3d vect_s, vect_m;
        EL scalar_m = complex_init_EL();

        vect_s = vect_sum(v1, v2, 1);
        vect_m = vect_mult(v1, v2, 1);
        scalar_m = scalar_mult(v1, v2, 1);


        printf("Vector sum: ");
        // (*(complex*)vect_s.x.data).R, 
        complex_pretty_print((*(complex*)vect_s.x.data).R, (*(complex*)vect_s.x.data).I, (*(complex*)vect_s.y.data).R, (*(complex*)vect_s.y.data).I,(*(complex*)vect_s.z.data).R, (*(complex*)vect_s.z.data).I);
        printf("Vector multiplication: ");
        complex_pretty_print((*(complex*)vect_m.x.data).R, (*(complex*)vect_m.x.data).I, (*(complex*)vect_m.y.data).R, (*(complex*)vect_m.y.data).I,(*(complex*)vect_m.z.data).R, (*(complex*)vect_m.z.data).I);
        printf("Scalar multiplication: {%f;%f}\n", (*(complex*)scalar_m.data).R, (*(complex*)scalar_m.data).I);

        free(values);

    }
    else{
        values = (float*)malloc(base_size);
        printf("Input first vector as 3 values for x,y,z:\n");
        scanf("%f %f %f", &values[0], &values[1], &values[2]); // , &values[3], &values[4], &values[5]
        
        printf("Input second vector as 3 values for x,y,z:\n");
        scanf("%f %f %f", &values[3], &values[4], &values[5]);

        printf("First vector:");
        float_pretty_print(values[0], values[1], values[2]);
        printf("Second vector:");
        float_pretty_print(values[3], values[4], values[5]);
        

        X1 = float_init_EL();
        Y1 = float_init_EL();
        Z1 = float_init_EL();
        X2 = float_init_EL();
        Y2 = float_init_EL();
        Z2 = float_init_EL();

        X1.data = &values[0];
        Y1.data = &values[1];
        Z1.data = &values[2];
        X2.data = &values[3];
        Y2.data = &values[4];
        Z2.data = &values[5];

        vector3d v1 = init_float_vector();
        vector3d v2 = init_float_vector();

        v1.x = X1;
        v1.y = Y1;
        v1.z = Z1;

        v2.x = X2;
        v2.y = Y2;
        v2.z = Z2;

        vector3d vect_s, vect_m;
        EL scalar_m = float_init_EL();

        vect_s = vect_sum(v1, v2, 0);
        vect_m = vect_mult(v1, v2, 0);
        scalar_m = scalar_mult(v1, v2, 0);

        printf("Vector sum: ");
        float_pretty_print(*(float*)vect_s.x.data, *(float*)vect_s.y.data, *(float*)vect_s.z.data);
        printf("Vector multiplication: ");
        float_pretty_print(*(float*)vect_m.x.data, *(float*)vect_m.y.data, *(float*)vect_m.z.data);
        printf("Scalar multiplication: %f\n", *(float*)scalar_m.data);

        free(values);
    }

}