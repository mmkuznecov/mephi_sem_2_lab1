#include <stdio.h>
#include <stdlib.h>

// declaration of structs

typedef struct complex complex;
typedef struct EL EL;
typedef struct vector3d vector3d;

struct complex{
    float R;
    float I;
};

struct EL{
    void* data; // data to process

    // arithmetic funcs as fields

    EL (*sum)(EL a, EL b);
    EL (*mult)(EL a, EL b);
    EL (*sub)(EL a, EL b);
};


struct vector3d {
    EL x;
    EL y;
    EL z;
};

// funcs declaration

EL float_init_EL();
EL complex_init_EL();
EL float_sum(EL c1, EL c2);
EL float_mult(EL c1, EL c2);
EL float_sub(EL c1, EL c2);
EL complex_sum(EL c1, EL c2);
EL complex_mult(EL c1, EL c2);
EL complex_sub(EL c1, EL c2);

// destuctor declaration

void complex_EL_destructor(EL complex_el);
void float_EL_destructor(EL float_EL);